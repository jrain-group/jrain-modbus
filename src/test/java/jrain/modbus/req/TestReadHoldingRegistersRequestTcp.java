package jrain.modbus.req;
import org.junit.Assert;
import org.junit.Test;

import jrain.modbus.cm.MbapHeader;
import jrain.modbus.req.ReadHoldingRegistersRequest;
import jrain.modbus.utils.ByteUtils;

public class TestReadHoldingRegistersRequestTcp {

	@Test
	public void testModubsTcp() {
		MbapHeader header = new MbapHeader(1, 0, 0);
		byte[] bytes = new ReadHoldingRegistersRequest(0, 0, 3).getTcpBytes(header);
		String str = ByteUtils.byteToHex(bytes);
		Assert.assertEquals("000100000008000300000003", str);

	}
}
